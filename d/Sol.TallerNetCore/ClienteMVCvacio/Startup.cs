﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ClienteMVCvacio
{
    public class Startup
    {

        public IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        // Todo funciona de arriba a abajo.

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        // Configurar que usar
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // Configuracion de los middleware
        // Here: Como se va a usar.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseStaticFiles(); // Usar archivos estaticos
            //app.UseMvcWithDefaultRoute(); // Pagina por defecto => Por defecto Home/Index

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync($"Hello World!: {Configuration.GetConnectionString("DefaultConnection")}" );
            });
        }
    }
}
