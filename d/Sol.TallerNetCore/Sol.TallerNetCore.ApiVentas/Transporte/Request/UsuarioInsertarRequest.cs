﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sol.TallerNetCore.ApiVentas.Transporte.Request
{
    public class UsuarioInsertarRequest
    {
        public String Nombres { get; set; }
        public String Correo { get; set; }
        public String Password { get; set; }
    }
}
