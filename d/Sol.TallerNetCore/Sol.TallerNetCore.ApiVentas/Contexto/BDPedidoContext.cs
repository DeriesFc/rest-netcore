﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Sol.TallerNetCore.ApiVentas.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sol.TallerNetCore.ApiVentas.Contexto
{
    public class BDPedidoContext : DbContext
    {

        public BDPedidoContext(DbContextOptions<BDPedidoContext> options) : base(options) {}

        public DbSet<Perfil> Perfil { get; set; }
        public DbSet<Usuario> Usuario { get; set; }

    }
}
