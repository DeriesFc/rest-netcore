﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sol.TallerNetCore.ApiVentas.DTO
{
    public class Usuario
    {
        [Key]
        public int IdUsuario { get; set; }
        public String Nombres { get; set; }
        public string Email { get; set; }
        public byte[] Password { get; set; }
        public string Image { get; set; }


    }
}
