﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sol.TallerNetCore.ApiVentas.Contexto;
using Sol.TallerNetCore.ApiVentas.DTO;

namespace Sol.TallerNetCore.ApiVentas.Dominio.UsuarioService
{
    public class UsuarioServiceBD : IUsuarioService
    {

        BDPedidoContext _context; 

        public UsuarioServiceBD(BDPedidoContext context)
        {
            this._context = context;
        }

        public Usuario Recuperar(int id)
        {
            return _context.Usuario
                .AsNoTracking()
                .Where((e) => e.IdUsuario == id).FirstOrDefault();
        }

        public List<Usuario> Listar()
        {
            return _context.Usuario.ToList();
        }

        public Usuario Insertar(Usuario usuario)
        {
            _context.Usuario.Add(usuario);
            _context.SaveChanges();
            return usuario;
        }

        public Usuario Actualizar(Usuario usuario)
        {
            _context.Usuario.Update(usuario);
            _context.SaveChanges();
            return usuario;
        }

        public bool Borrar(int id) { throw new NotImplementedException(); }

    }
}
