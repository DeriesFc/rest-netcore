﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sol.TallerNetCore.ApiVentas.Contexto;
using Sol.TallerNetCore.ApiVentas.DTO;

namespace Sol.TallerNetCore.ApiVentas.Dominio.PerfilService
{
    public class PerfilServiceBD : IPerfilService
    {

        BDPedidoContext _context;

        public PerfilServiceBD(BDPedidoContext context)
        {
            this._context = context;
        }

        public Perfil Recuperar(int id) {
            return _context.Perfil.AsNoTracking().Where((t) => t.IdPerfil == id).FirstOrDefault();
        }

        public List<Perfil> Listar() {
            return _context.Perfil.ToList();
        }

        public Perfil Insertar(Perfil perfil)
        {
            _context.Perfil.Add(perfil);
            _context.SaveChanges();
            return perfil;
        }

        public Perfil Actualizar(Perfil perfil)
        {
            _context.Perfil.Update(perfil);
            _context.SaveChanges();
            return perfil;
        }
        public bool Eliminar(int id) { throw new NotImplementedException(); }

    }
}
