﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sol.TallerNetCore.ApiVentas.DTO;

namespace Sol.TallerNetCore.ApiVentas.Dominio.PerfilService
{
    public class PerfilServiceMemoria : IPerfilService
    {

        List<Perfil> _Lista;

        public PerfilServiceMemoria()
        {
            _Lista = new List<Perfil>()
            {
                new Perfil() { IdPerfil= 1, NombrePerfil = "Pruebas"},
                new Perfil() { IdPerfil= 2, NombrePerfil = "Produccion"}
            };
        }


        public Perfil Recuperar(int id) { return _Lista.Where(t => t.IdPerfil == id).FirstOrDefault(); }

        public List<Perfil> Listar() { return _Lista; }

        public Perfil Actualizar(Perfil perfil) { throw new NotImplementedException(); }
        public bool Eliminar(int id) { throw new NotImplementedException(); }
        public Perfil Insertar(Perfil perfil) { throw new NotImplementedException(); }

    }
}
