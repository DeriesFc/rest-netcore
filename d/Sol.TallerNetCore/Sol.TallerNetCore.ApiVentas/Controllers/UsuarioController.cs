﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sol.TallerNetCore.ApiVentas.Dominio.UsuarioService;
using Sol.TallerNetCore.ApiVentas.DTO;
using Sol.TallerNetCore.ApiVentas.Transporte.Request;
using Sol.TallerNetCore.ApiVentas.Utiles;

namespace Sol.TallerNetCore.ApiVentas.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class UsuarioController : Controller
    {
        private IUsuarioService _usuarioService;

        public UsuarioController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        [HttpGet]
        public IActionResult Listar()
        {
            return StatusCode(200, _usuarioService.Listar());
        }

        [HttpPost]
        public IActionResult Insertar([FromBody]UsuarioInsertarRequest usuarioInsertarRequest)
        {

            if (string.IsNullOrWhiteSpace(usuarioInsertarRequest.Password)) {
                return BadRequest("Ingrese su clave");
            }

            Usuario usuario = new Usuario();
            usuario.Nombres = usuarioInsertarRequest.Nombres;
            usuario.Email = usuarioInsertarRequest.Correo;
            usuario.Password = EncriptadorHelper.EncryptToByte(usuarioInsertarRequest.Password);

            return Ok(_usuarioService.Insertar(usuario));
        }
    }
}