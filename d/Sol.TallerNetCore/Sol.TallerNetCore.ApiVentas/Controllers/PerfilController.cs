﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sol.TallerNetCore.ApiVentas.Dominio.PerfilService;
using Sol.TallerNetCore.ApiVentas.DTO;

namespace Sol.TallerNetCore.ApiVentas.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class PerfilController : Controller
    {

        private IPerfilService _perfilService;

        public PerfilController(IPerfilService perfilService)
        {
            _perfilService = perfilService;
        }

        [HttpGet]
        public List<Perfil> ListarPerfiles()
        {
            return _perfilService.Listar();
        }
        [HttpPost]
        public Perfil Insertar([FromBody]Perfil perfil)
        {
            return _perfilService.Insertar(perfil);
        }

        //[HttpPatch]
        [HttpPut]
        public IActionResult Actualizar( int id, [FromBody] Perfil perfil)
        {
            Perfil exists = _perfilService.Recuperar(id);

            if (exists == null) { return BadRequest("El código no existe"); }
            perfil.IdPerfil = id;
            _perfilService.Actualizar(perfil);

            return StatusCode(200, perfil);

        }
    }
}